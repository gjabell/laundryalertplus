package com.galenabell.laundryalertplus.di.modules

import com.galenabell.laundryalertplus.di.scopes.ActivityScope
import com.galenabell.laundryalertplus.ui.activities.LocationSelectionActivity
import com.galenabell.laundryalertplus.ui.activities.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivitiesModule {
    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributesMainActivity(): MainActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun contributesLocationSelectionActivity(): LocationSelectionActivity
}
