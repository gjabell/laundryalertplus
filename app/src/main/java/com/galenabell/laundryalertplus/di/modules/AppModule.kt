package com.galenabell.laundryalertplus.di.modules

import android.arch.persistence.room.Room
import com.galenabell.laundryalertplus.AppConstants
import com.galenabell.laundryalertplus.di.qualifiers.DatabaseQualifier
import com.galenabell.laundryalertplus.App
import com.galenabell.laundryalertplus.database.database.AppDatabase
import com.galenabell.laundryalertplus.models.model.Location
import com.galenabell.laundryalertplus.models.util.LiveDataCallAdapterFactory
import com.galenabell.laundryalertplus.models.util.LocationDeserializer
import com.galenabell.laundryalertplus.models.util.RoomDeserializer
import com.galenabell.laundryalertplus.network.retrofit.WebService
import com.google.gson.*
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module(includes = [ViewModelsModule::class])
class AppModule {
    @Provides
    @Singleton
    fun providesLocationService(): WebService =
            Retrofit.Builder()
                    .baseUrl(AppConstants.API_URL)
                    .addConverterFactory(GsonConverterFactory.create(GsonBuilder()
//                            .registerTypeAdapter(Location::class.java, LocationDeserializer())
//                            .registerTypeAdapter(Room::class.java, RoomDeserializer())
                            .create()))
                    .addCallAdapterFactory(LiveDataCallAdapterFactory())
                    .build()
                    .create(WebService::class.java)

    @Provides
    @Singleton
    @DatabaseQualifier
    fun providesAppDatabase(app: App): AppDatabase =
            Room
                    .databaseBuilder(app, AppDatabase::class.java, AppConstants.DB_NAME)
                    .fallbackToDestructiveMigration()
                    .build()

    @Provides
    @Singleton
    fun providesLocationDao(@DatabaseQualifier database: AppDatabase) = database.locationDao()

    @Provides
    @Singleton
    fun providesRoomDao(@DatabaseQualifier database: AppDatabase) = database.roomDao()

    @Provides
    @Singleton
    fun providesMachineDao(@DatabaseQualifier database: AppDatabase) = database.machineDao()
}
