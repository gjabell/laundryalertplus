package com.galenabell.laundryalertplus.di.qualifiers

import javax.inject.Qualifier

@Qualifier
annotation class ApplicationQualifier