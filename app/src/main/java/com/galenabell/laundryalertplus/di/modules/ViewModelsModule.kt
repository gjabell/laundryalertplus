package com.galenabell.laundryalertplus.di.modules

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.galenabell.laundryalertplus.di.ViewModelKey
import com.galenabell.laundryalertplus.models.viewmodel.LocationViewModel
import com.galenabell.laundryalertplus.models.viewmodel.MachinesViewModel
import com.galenabell.laundryalertplus.models.viewmodel.RoomsViewModel
import com.galenabell.laundryalertplus.models.viewmodel.ViewModelFactory
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
internal abstract class ViewModelsModule {
    @Binds
    @IntoMap
    @ViewModelKey(LocationViewModel::class)
    abstract fun bindLocationViewModel(viewModel: LocationViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RoomsViewModel::class)
    abstract fun bindRoomsViewModel(viewModel: RoomsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MachinesViewModel::class)
    abstract fun bindMachinesViewModel(viewModel: MachinesViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(viewModelFactory: ViewModelFactory): ViewModelProvider.Factory
}
