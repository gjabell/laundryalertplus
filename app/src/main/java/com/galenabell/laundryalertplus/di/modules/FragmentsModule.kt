package com.galenabell.laundryalertplus.di.modules

import com.galenabell.laundryalertplus.di.scopes.FragmentScope
import com.galenabell.laundryalertplus.ui.fragments.HomeFragment
import com.galenabell.laundryalertplus.ui.fragments.MachinesFragment
import com.galenabell.laundryalertplus.ui.fragments.RoomsFragment
import com.galenabell.laundryalertplus.ui.fragments.SettingsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentsModule {
    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributesHomeFragment(): HomeFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributesRoomsFragment(): RoomsFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributesMachinesFragment(): MachinesFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributesSettingsFragment(): SettingsFragment
}
