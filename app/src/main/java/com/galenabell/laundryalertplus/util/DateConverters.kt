package com.galenabell.laundryalertplus.util

import android.arch.persistence.room.TypeConverter
import java.util.*

object DateConverters {
    @TypeConverter
    @JvmStatic
    fun dateFromTimestamp(timestamp: Long?): Date? {
        return if (timestamp == null) null else Date(timestamp)
    }

    @TypeConverter
    @JvmStatic
    fun dateToTimestamp(date: Date?): Long? {
        return date?.time
    }
}
