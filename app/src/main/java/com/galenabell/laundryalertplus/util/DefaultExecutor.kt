package com.galenabell.laundryalertplus.util

import java.util.concurrent.Executor

class DefaultExecutor : Executor {
    override fun execute(command: Runnable?) {
        Thread(command).run()
    }
}