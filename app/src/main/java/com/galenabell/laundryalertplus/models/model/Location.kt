package com.galenabell.laundryalertplus.models.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "locations")
data class Location(
        @PrimaryKey @ColumnInfo(name = "code") var code: String,
        @ColumnInfo(name = "name") var name: String,
        @ColumnInfo(name = "num_rooms") var numRooms: Int,
        @ColumnInfo(name = "selected") var selected: Boolean) {
    override fun equals(other: Any?): Boolean {
        if (other is Location)
            return this.code == other.code
        return false
    }

    override fun hashCode(): Int {
        return super.hashCode()
    }
}
