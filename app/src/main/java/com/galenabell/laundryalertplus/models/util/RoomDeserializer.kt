package com.galenabell.laundryalertplus.models.util

import com.galenabell.laundryalertplus.models.model.Machine
import com.galenabell.laundryalertplus.models.model.Room
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import java.lang.reflect.Type
import java.text.SimpleDateFormat
import java.util.*

class RoomDeserializer /*: JsonDeserializer<RoomWrapper>*/ {
    /*private val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US)

    override fun deserialize(e: JsonElement?, type: Type?, context: JsonDeserializationContext?): RoomWrapper {
        val w = e?.asJsonObject?.get("location")?.asJsonObject
        val name = w?.get("name")?.asString ?: ""
        val code = w?.get("code")?.asString ?: ""
        val rooms = ArrayList<Room>()
        w?.get("rooms")?.asJsonArray?.forEach { rooms.add(deserializeRoom(it)) }
        return RoomWrapper(name, code, rooms)
    }

    private fun deserializeRoom(e: JsonElement?): Room {
        val r = e?.asJsonObject
        val name = r?.get("name")?.asString ?: ""
        val machines = ArrayList<Machine>()
        val room = Room(name, false)
        room.machines = machines
        r?.get("machines")?.asJsonArray?.forEach { machines.add(deserializeMachine(it, name)) }
        return room
    }

    private fun deserializeMachine(e: JsonElement?, room: String): Machine {
        val m = e?.asJsonObject
        val label = m?.get("label")?.asString ?: ""
        val description = m?.get("description")?.asString ?: ""
        val status = m?.get("status")?.asString ?: ""
        val startTime = format.parse(m?.get("startTime")?.asString)
        val timeRemaining = m?.get("timeRemaining")?.asString ?: ""
        return Machine(room, label, description, status, startTime, timeRemaining)
    }*/
}
