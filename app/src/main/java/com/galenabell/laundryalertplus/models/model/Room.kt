package com.galenabell.laundryalertplus.models.model

import android.arch.persistence.room.*

@Entity(tableName = "rooms")
data class Room(
        @PrimaryKey @ColumnInfo(name = "name") var name: String,
        @ColumnInfo(name = "location_code") var locationCode: String,
        @ColumnInfo(name = "num_machines") var numMachines: Int,
        @ColumnInfo(name = "num_available") var numAvailable: Int,
        @ColumnInfo(name = "favorite") var favorite: Boolean
) {
    override fun equals(other: Any?): Boolean {
        if (other is Room)
            return this.name == other.name
        return false
    }

    override fun hashCode(): Int {
        return super.hashCode()
    }
}
