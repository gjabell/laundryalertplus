package com.galenabell.laundryalertplus.models.util

import com.galenabell.laundryalertplus.models.model.Location
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import java.lang.reflect.Type

class LocationDeserializer : JsonDeserializer<Location> {
    override fun deserialize(e: JsonElement?, type: Type?, context: JsonDeserializationContext?): Location {
        val l = e?.asJsonObject?.get("location_masters")?.asJsonObject
        val id = l?.get("id")?.asString ?: 0
        val code = l?.get("code")?.asString ?: ""
        val name = l?.get("name")?.asString ?: ""
        return Location(code, name, 10,false)
    }
}
