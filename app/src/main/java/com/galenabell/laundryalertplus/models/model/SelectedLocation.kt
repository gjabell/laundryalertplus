package com.galenabell.laundryalertplus.models.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "selectedLocations")
data class SelectedLocation(
        @PrimaryKey
        @ColumnInfo(name = "code")
        var code: String
)
