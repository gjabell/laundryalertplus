package com.galenabell.laundryalertplus.models.model

enum class Status {
    SUCCESS,
    LOADING,
    ERROR
}
