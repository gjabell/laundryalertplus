package com.galenabell.laundryalertplus.models.model

import android.arch.persistence.room.*
import com.galenabell.laundryalertplus.util.DateConverters
import java.util.*

@Entity(tableName = "machines",
        indices = [Index(
                value = ["room_name"]
        )])
@TypeConverters(DateConverters::class)
data class Machine(
        @ColumnInfo(name = "room_name") var roomName: String,
        @ColumnInfo(name = "label") var label: String,
        @ColumnInfo(name = "description") var description: String,
        @ColumnInfo(name = "status") var status: String,
        @ColumnInfo(name = "startTime") var startTime: Date,
        @ColumnInfo(name = "timeRemaining") var timeRemaining: String,
        @ColumnInfo(name = "favorite") var favorite: Boolean) {
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0

    override fun equals(other: Any?): Boolean {
        if (other is Machine)
            return this.roomName == other.roomName && this.label == other.label
        return false
    }

    override fun hashCode(): Int {
        return super.hashCode()
    }
}
