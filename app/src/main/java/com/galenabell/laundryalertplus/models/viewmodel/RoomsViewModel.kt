package com.galenabell.laundryalertplus.models.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import android.arch.lifecycle.ViewModel
import com.galenabell.laundryalertplus.models.model.Location
import com.galenabell.laundryalertplus.models.model.Resource
import com.galenabell.laundryalertplus.models.model.Room
import com.galenabell.laundryalertplus.network.LocationRepository
import com.galenabell.laundryalertplus.network.RoomRepository
import javax.inject.Inject

class RoomsViewModel @Inject constructor(locationRepo: LocationRepository, private val roomRepo: RoomRepository) : ViewModel() {
    private val locationCode = MutableLiveData<String>()

    var locations: LiveData<Resource<List<Location>>> = locationRepo.loadLocations()
    var rooms: LiveData<Resource<List<Room>>> = Transformations
            .switchMap(locationCode) {
                roomRepo.loadRooms(it)
            }

    fun setCode(code: String) {
        locationCode.value = code
    }
}
