package com.galenabell.laundryalertplus.models.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import android.arch.lifecycle.ViewModel
import com.galenabell.laundryalertplus.models.model.Machine
import com.galenabell.laundryalertplus.models.model.Resource
import com.galenabell.laundryalertplus.network.MachineRepository
import javax.inject.Inject

class MachinesViewModel @Inject constructor(private val machineRepo: MachineRepository) : ViewModel() {
    private val roomName = MutableLiveData<String>()

    val machines: LiveData<Resource<List<Machine>>> = Transformations
            .switchMap(roomName) {
                machineRepo.loadMachines(it)
            }

    fun setName(name: String) {
        roomName.value = name
    }
}
