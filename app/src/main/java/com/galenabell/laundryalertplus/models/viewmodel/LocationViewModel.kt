package com.galenabell.laundryalertplus.models.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import com.galenabell.laundryalertplus.models.model.Location
import com.galenabell.laundryalertplus.models.model.Resource
import com.galenabell.laundryalertplus.network.LocationRepository
import javax.inject.Inject

class LocationViewModel @Inject constructor(repository: LocationRepository) : ViewModel() {
    var locations: LiveData<Resource<List<Location>>> = repository.loadLocations()
}
