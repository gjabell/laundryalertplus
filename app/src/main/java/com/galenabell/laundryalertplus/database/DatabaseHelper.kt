package com.galenabell.laundryalertplus.database

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MediatorLiveData
import android.arch.lifecycle.Transformations
import com.galenabell.laundryalertplus.AppExecutors
import com.galenabell.laundryalertplus.database.dao.MachineDao
import com.galenabell.laundryalertplus.database.dao.RoomDao
import com.galenabell.laundryalertplus.models.model.Room
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DatabaseHelper @Inject constructor(
        private val roomDao: RoomDao,
        private val machineDao: MachineDao) {
//    fun getRoomsWithMachines(): LiveData<List<Room>> {
//        var roomsLiveData = roomDao.getRooms()
//        roomsLiveData = Transformations.switchMap(roomsLiveData, { rooms ->
//            val mediatorLiveData = MediatorLiveData<List<Room>>()
//            rooms.forEach { room ->
//                mediatorLiveData.addSource(machineDao.getMachines(room.name), { machines ->
//                    if (machines != null) {
//                        room.machines = machines
//                        mediatorLiveData.postValue(rooms)
//                    }
//                })
//            }
//            mediatorLiveData
//        })
//        return roomsLiveData
//    }
}
