package com.galenabell.laundryalertplus.database.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import com.galenabell.laundryalertplus.models.model.Room

@Dao
interface RoomDao {
    @Query("SELECT * FROM rooms ORDER BY name")
    fun getRooms(): LiveData<List<Room>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertRoom(room: Room)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertRooms(rooms: List<Room>)

    @Delete
    fun deleteRoom(room: Room)
}
