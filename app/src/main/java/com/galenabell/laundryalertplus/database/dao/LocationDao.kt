package com.galenabell.laundryalertplus.database.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import com.galenabell.laundryalertplus.models.model.Location

@Dao
interface LocationDao {
    @Query("SELECT * FROM locations")
    fun getLocations(): LiveData<List<Location>>

    @Query("SELECT * FROM locations WHERE selected = 1 LIMIT 1")
    fun getSelectedLocation(): LiveData<Location>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertLocations(locations: List<Location>)

    @Update
    fun updateLocation(location: Location)
}
