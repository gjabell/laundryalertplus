package com.galenabell.laundryalertplus.database.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.galenabell.laundryalertplus.database.dao.LocationDao
import com.galenabell.laundryalertplus.database.dao.MachineDao
import com.galenabell.laundryalertplus.database.dao.RoomDao
import com.galenabell.laundryalertplus.models.model.Location
import com.galenabell.laundryalertplus.models.model.Machine

@Database(entities = [Location::class, com.galenabell.laundryalertplus.models.model.Room::class, Machine::class], version = 8)
abstract class AppDatabase : RoomDatabase() {
    abstract fun locationDao(): LocationDao
    abstract fun roomDao(): RoomDao
    abstract fun machineDao(): MachineDao
}
