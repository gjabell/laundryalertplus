package com.galenabell.laundryalertplus.database.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import com.galenabell.laundryalertplus.models.model.Machine

@Dao
interface MachineDao {
    @Query("SELECT * FROM machines")
    fun getMachines(): LiveData<List<Machine>>

    @Query("SELECT * FROM machines WHERE room_name = :name")
    fun getMachines(name: String): LiveData<List<Machine>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMachine(machine: Machine)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMachines(machines: List<Machine>)

    @Update
    fun updateMachine(machine: Machine)

    @Delete
    fun deleteMachine(machine: Machine)
}
