package com.galenabell.laundryalertplus.ui.activities

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.AsyncTask
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.galenabell.laundryalertplus.R
import com.galenabell.laundryalertplus.database.dao.LocationDao
import com.galenabell.laundryalertplus.models.model.Location
import com.galenabell.laundryalertplus.models.model.Status
import com.galenabell.laundryalertplus.models.viewmodel.LocationViewModel
import com.galenabell.laundryalertplus.models.viewmodel.ViewModelFactory
import com.galenabell.laundryalertplus.ui.adapters.LocationViewAdapter
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.activity_location_selection.*
import javax.inject.Inject

class LocationSelectionActivity : AppCompatActivity(), HasSupportFragmentInjector, LocationViewAdapter.LocationActionListener {
    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector() = dispatchingAndroidInjector

    @Inject
    lateinit var locationDao: LocationDao

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var locationViewModel: LocationViewModel
    private lateinit var viewAdapter: LocationViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_location_selection)

        supportActionBar?.title = getString(R.string.location_selection_title)

        viewAdapter = LocationViewAdapter(this, applicationContext)

        recyclerView.apply {
            setHasFixedSize(true)

            layoutManager = LinearLayoutManager(this@LocationSelectionActivity)

            adapter = viewAdapter
        }

        locationViewModel = ViewModelProviders.of(this, viewModelFactory).get(LocationViewModel::class.java)
        locationViewModel.locations.observe(this, Observer {
            it?.let {
                if (it.status == Status.SUCCESS) {
                    it.data?.let {
                        render(it)
                    }
                }
            }
        })
    }

    private fun render(locations: List<Location>) {
        when (locations.isEmpty()) {
            true -> {
                progressBar.visibility = View.VISIBLE
                recyclerView.visibility = View.GONE
            }
            false -> {
                progressBar.visibility = View.GONE
                recyclerView.visibility = View.VISIBLE
                viewAdapter.locations = locations
            }
        }
    }

    override fun onClick(location: Location) {
        AsyncTask.execute {
            runOnUiThread { progressBar.visibility = View.VISIBLE }
            location.selected = true
            locationDao.updateLocation(location)
            runOnUiThread { this.finish() }
        }
    }
}
