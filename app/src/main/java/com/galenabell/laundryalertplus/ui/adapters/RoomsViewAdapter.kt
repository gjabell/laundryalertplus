package com.galenabell.laundryalertplus.ui.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.galenabell.laundryalertplus.databinding.RoomListItemBinding
import com.galenabell.laundryalertplus.models.model.Room
import kotlinx.android.synthetic.main.room_list_item.view.*

class RoomsViewAdapter(private val listener: RoomActionListener, private val context: Context) : RecyclerView.Adapter<RoomsViewAdapter.RowHolder>() {
    interface RoomActionListener {
        fun onClick(room: Room)
        fun onFavorite(room: Room, isFavorite: Boolean)
    }

    var rooms: List<Room> = ArrayList()

    class RowHolder(private val binding: RoomListItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(room: Room, favorite: Boolean) {
            binding.room = room
            binding.favorite = favorite

            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RoomsViewAdapter.RowHolder {
        return RowHolder(RoomListItemBinding.inflate(LayoutInflater.from(context), parent, false))
    }

    override fun onBindViewHolder(holder: RowHolder, position: Int) {
        val room = rooms[position]
        holder.bind(room, false)
        holder.itemView.setOnClickListener { listener.onClick(room) }
        holder.itemView.favoriteView.setOnClickListener { listener.onFavorite(room, false) }
    }

    override fun getItemCount() = rooms.size
}
