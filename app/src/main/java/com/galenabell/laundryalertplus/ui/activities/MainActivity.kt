package com.galenabell.laundryalertplus.ui.activities

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.view.View
import com.galenabell.laundryalertplus.R
import com.galenabell.laundryalertplus.database.dao.LocationDao
import com.galenabell.laundryalertplus.models.model.Location
import com.galenabell.laundryalertplus.ui.adapters.NavigationAdapter
import com.galenabell.laundryalertplus.ui.fragments.HomeFragment
import com.galenabell.laundryalertplus.ui.fragments.RoomsFragment
import com.galenabell.laundryalertplus.ui.fragments.SettingsFragment
import dagger.android.AndroidInjection
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity(), HasSupportFragmentInjector, NavigationAdapter.OnItemClickListener {
    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun supportFragmentInjector() = dispatchingAndroidInjector

    @Inject
    lateinit var locationDao: LocationDao

    private lateinit var actionBar: Toolbar
    private lateinit var drawerLayout: DrawerLayout
    //    private lateinit var progressBar: ProgressBar
    private lateinit var drawerList: RecyclerView
    private lateinit var drawerToggle: ActionBarDrawerToggle
    private lateinit var newTitle: CharSequence
    private lateinit var drawerTitles: Array<String>
    private lateinit var homeTitle: CharSequence

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)

        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        homeTitle = ""

        actionBar = findViewById(R.id.toolbar)
//        progressBar = findViewById(R.id.progressBar)

        drawerTitles = resources.getStringArray(R.array.navigation_titles)
        drawerLayout = findViewById<DrawerLayout>(R.id.drawerLayout).apply {
            setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START)
        }

        drawerList = findViewById<RecyclerView>(R.id.drawer).apply {
            setHasFixedSize(true)
            adapter = NavigationAdapter(drawerTitles, this@MainActivity)
        }

        setSupportActionBar(toolbar)
        supportActionBar?.run {
            setDisplayHomeAsUpEnabled(true)
            setHomeButtonEnabled(true)
        }

        drawerToggle = object : ActionBarDrawerToggle(
                this,
                drawerLayout,
                R.string.drawer_open,
                R.string.drawer_close
        ) {
            override fun onDrawerClosed(drawerView: View) {
                invalidateOptionsMenu()
            }

            override fun onDrawerOpened(drawerView: View) {
                invalidateOptionsMenu()
            }
        }

        drawerLayout.addDrawerListener(drawerToggle)

        if (savedInstanceState == null) selectItem(0)
    }

    override fun onResume() {
        super.onResume()
        locationDao.getSelectedLocation().observe(this, Observer {
            setup(it)
        })
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (drawerToggle.onOptionsItemSelected(item)) return true
        return super.onOptionsItemSelected(item)
    }

    private fun setup(location: Location?) {
        location ?: startActivity(Intent(this, LocationSelectionActivity::class.java))
        homeTitle = String.format("%s %s", location?.name, getString(R.string.laundry))
        title = homeTitle
    }

    override fun setTitle(title: CharSequence) {
        newTitle = title
        supportActionBar?.title = title
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        drawerToggle.syncState()
    }

    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)
        drawerToggle.onConfigurationChanged(newConfig)
    }

    override fun onClick(view: View, position: Int) {
        selectItem(position)
    }

    @SuppressLint("CommitTransaction")
    private fun selectItem(position: Int) {
        supportFragmentManager.beginTransaction().run {
            replace(R.id.frameLayout, when (position) {
                1 -> RoomsFragment()
                2 -> SettingsFragment()
                else -> HomeFragment()
            })
            commit()
        }
        title = if (position == 0) homeTitle else drawerTitles[position]
        drawerLayout.closeDrawer(drawerList)
    }
}
