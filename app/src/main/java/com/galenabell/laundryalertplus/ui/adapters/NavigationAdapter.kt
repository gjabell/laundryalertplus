package com.galenabell.laundryalertplus.ui.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.galenabell.laundryalertplus.R

class NavigationAdapter(private val data: Array<String>, private val listener: OnItemClickListener) : RecyclerView.Adapter<NavigationAdapter.ViewHolder>() {
    interface OnItemClickListener {
        fun onClick(view: View, position: Int)
    }

    class ViewHolder(val textView: TextView) : RecyclerView.ViewHolder(textView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
            ViewHolder(LayoutInflater.from(parent.context)
                    .inflate(R.layout.navigation_list_item, parent, false)
                    .findViewById(R.id.title))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.apply {
            textView.text = data[position]
            textView.setOnClickListener { view -> listener.onClick(view, position) }
        }
    }

    override fun getItemCount() = data.size
}
