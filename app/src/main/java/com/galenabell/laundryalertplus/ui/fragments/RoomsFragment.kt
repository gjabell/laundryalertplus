package com.galenabell.laundryalertplus.ui.fragments

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.AsyncTask
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.galenabell.laundryalertplus.R
import com.galenabell.laundryalertplus.database.dao.RoomDao
import com.galenabell.laundryalertplus.models.model.Room
import com.galenabell.laundryalertplus.models.model.Status
import com.galenabell.laundryalertplus.models.viewmodel.RoomsViewModel
import com.galenabell.laundryalertplus.models.viewmodel.ViewModelFactory
import com.galenabell.laundryalertplus.ui.adapters.RoomsViewAdapter
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_room.*
import kotlinx.android.synthetic.main.fragment_room.view.*
import javax.inject.Inject

class RoomsFragment : DaggerFragment(), RoomsViewAdapter.RoomActionListener {
    @Inject
    lateinit var roomDao: RoomDao

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var viewModel: RoomsViewModel
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RoomsViewAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewAdapter = RoomsViewAdapter(this, requireContext())

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(RoomsViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_room, container, false)
        recyclerView = v.recyclerView.apply {
            setHasFixedSize(true)

            layoutManager = LinearLayoutManager(activity)

            adapter = viewAdapter
        }
        return v
    }

    override fun onResume() {
        super.onResume()

        viewModel.locations.observe(this, Observer {
            it?.let {
                if (it.status == Status.SUCCESS) {
                    it.data?.let {
                        viewModel.setCode(it.first { l -> l.selected }.code)
                    }
                }
            }
        })

        viewModel.rooms.observe(this, Observer {
            it?.let {
                when (it.status) {
                    Status.SUCCESS -> it.data?.let {
                        viewAdapter.rooms = it
                        viewAdapter.notifyDataSetChanged()
                        onLoaded()
                    }
                    Status.LOADING -> {
                    }
                    Status.ERROR -> {
                        Log.d("RoomsFragment", it.message)
                    }
                }
            }
        })
    }

    private fun onLoaded() {
        progressBar.visibility = View.GONE
        recyclerView.visibility = View.VISIBLE
    }

    override fun onClick(room: Room) {
        (activity as AppCompatActivity).run {
            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.frameLayout, MachinesFragment.init(room.name))
                    .addToBackStack(null)
                    .commit()
        }
    }

    override fun onFavorite(room: Room, isFavorite: Boolean) {
        AsyncTask.execute {
            when (isFavorite) {
                true -> roomDao.deleteRoom(room)
                false -> roomDao.insertRoom(room)
            }
        }
    }
}
