package com.galenabell.laundryalertplus.ui.fragments

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.AsyncTask
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.galenabell.laundryalertplus.R
import com.galenabell.laundryalertplus.database.dao.MachineDao
import com.galenabell.laundryalertplus.models.model.Machine
import com.galenabell.laundryalertplus.models.model.Status
import com.galenabell.laundryalertplus.models.viewmodel.MachinesViewModel
import com.galenabell.laundryalertplus.models.viewmodel.ViewModelFactory
import com.galenabell.laundryalertplus.ui.adapters.MachinesViewAdapter
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_machine.*
import kotlinx.android.synthetic.main.fragment_room.view.*
import javax.inject.Inject

class MachinesFragment : DaggerFragment(), MachinesViewAdapter.MachineActionListener {
    @Inject
    lateinit var machineDao: MachineDao

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var viewModel: MachinesViewModel
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: MachinesViewAdapter
    private lateinit var roomName: String

    companion object {
        fun init(roomName: String): MachinesFragment {
            val fragment = MachinesFragment()
            fragment.roomName = roomName
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewAdapter = MachinesViewAdapter(this, requireContext())

        viewModel = ViewModelProviders.of(this, viewModelFactory).get(MachinesViewModel::class.java)
        viewModel.setName(roomName)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_machine, container, false)
        recyclerView = v.recyclerView.apply {
            setHasFixedSize(true)

            layoutManager = LinearLayoutManager(activity)

            adapter = viewAdapter
        }
        return v
    }

    override fun onResume() {
        super.onResume()

        viewModel.machines.observe(this, Observer {
            it?.let {
                when (it.status) {
                    Status.SUCCESS -> it.data?.let {
                        viewAdapter.machines = it
                        viewAdapter.notifyDataSetChanged()
                        onLoaded()
                    }
                    Status.LOADING -> {
                    }
                    Status.ERROR -> {
                        Log.d("MachinesFragment", it.message)
                    }
                }
            }
        })
    }

    private fun onLoaded() {
        progressBar.visibility = View.GONE
        recyclerView.visibility = View.VISIBLE
    }

    override fun onFavorite(machine: Machine) {
        AsyncTask.execute {
            when (machine.favorite) {
                true -> machineDao.deleteMachine(machine)
                false -> machineDao.insertMachine(machine)
            }
        }
    }
}
