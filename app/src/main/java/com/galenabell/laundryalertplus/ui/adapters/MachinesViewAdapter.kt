package com.galenabell.laundryalertplus.ui.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.galenabell.laundryalertplus.databinding.MachineListItemBinding
import com.galenabell.laundryalertplus.models.model.Machine
import kotlinx.android.synthetic.main.room_list_item.view.*

class MachinesViewAdapter(private val listener: MachineActionListener, private val context: Context) : RecyclerView.Adapter<MachinesViewAdapter.RowHolder>() {
    interface MachineActionListener {
        fun onFavorite(machine: Machine)
    }

    var machines: List<Machine> = ArrayList()

    class RowHolder(private val binding: MachineListItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(machine: Machine) {
            binding.machine = machine

            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RowHolder {
        return RowHolder(MachineListItemBinding.inflate(LayoutInflater.from(context), parent, false))
    }

    override fun onBindViewHolder(holder: RowHolder, position: Int) {
        val machine = machines[position]
        holder.bind(machine)
        holder.itemView.favoriteView.setOnClickListener { listener.onFavorite(machine) }
    }

    override fun getItemCount() = machines.size
}
