package com.galenabell.laundryalertplus.ui.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.galenabell.laundryalertplus.databinding.LocationListItemBinding
import com.galenabell.laundryalertplus.models.model.Location

class LocationViewAdapter(private val listener: LocationActionListener, private val context: Context) : RecyclerView.Adapter<LocationViewAdapter.RowHolder>() {
    interface LocationActionListener {
        fun onClick(location: Location)
    }

    var locations: List<Location> = ArrayList()

    class RowHolder(private val binding: LocationListItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(location: Location) {
            binding.location = location

            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LocationViewAdapter.RowHolder {
        return RowHolder(LocationListItemBinding.inflate(LayoutInflater.from(context), parent, false))
    }

    override fun onBindViewHolder(holder: RowHolder, position: Int) {
        val location = locations[position]
        holder.bind(location)
        holder.itemView.setOnClickListener { listener.onClick(location) }
    }

    override fun getItemCount() = locations.size
}
