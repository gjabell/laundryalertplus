package com.galenabell.laundryalertplus.network

import android.arch.lifecycle.LiveData
import com.galenabell.laundryalertplus.AppExecutors
import com.galenabell.laundryalertplus.database.dao.RoomDao
import com.galenabell.laundryalertplus.models.model.Resource
import com.galenabell.laundryalertplus.models.model.Room
import com.galenabell.laundryalertplus.network.retrofit.WebService
import com.galenabell.laundryalertplus.util.RateLimiter
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RoomRepository @Inject constructor(
        private val appExecutors: AppExecutors,
        private val roomDao: RoomDao,
        private val webService: WebService
) {
    private val roomsListRateLimit = RateLimiter<String>(1, TimeUnit.MINUTES)

    fun loadRooms(code: String): LiveData<Resource<List<Room>>> =
            object : NetworkBoundResource<List<Room>, List<Room>>(appExecutors) {
                override fun saveCallResult(item: List<Room>) {
                    roomDao.insertRooms(item)
                }

                override fun shouldFetch(data: List<Room>?): Boolean {
                    return data == null || data.isEmpty() || roomsListRateLimit.shouldFetch(code)
                }

                override fun loadFromDb() = roomDao.getRooms()

                override fun createCall() = webService.getRooms(code)

                override fun onFetchFailed() {
                    roomsListRateLimit.reset(code)
                }
            }.asLiveData()
}
