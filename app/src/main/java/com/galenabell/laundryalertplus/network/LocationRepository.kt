package com.galenabell.laundryalertplus.network

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.galenabell.laundryalertplus.AppExecutors
import com.galenabell.laundryalertplus.database.dao.LocationDao
import com.galenabell.laundryalertplus.models.model.Location
import com.galenabell.laundryalertplus.models.model.Resource
import com.galenabell.laundryalertplus.network.retrofit.WebService
import com.galenabell.laundryalertplus.util.RateLimiter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LocationRepository @Inject constructor(
        private val appExecutors: AppExecutors,
        private val locationDao: LocationDao,
        private val webService: WebService) {
    private val locationsListRateLimit = RateLimiter<String>(10, TimeUnit.MINUTES)

    fun loadLocations(): LiveData<Resource<List<Location>>> =
            object : NetworkBoundResource<List<Location>, List<Location>>(appExecutors) {
                override fun saveCallResult(item: List<Location>) {
                    locationDao.insertLocations(item)
                }

                override fun shouldFetch(data: List<Location>?): Boolean {
                    return data == null || data.isEmpty() || locationsListRateLimit.shouldFetch("")
                }

                override fun loadFromDb() = locationDao.getLocations()

                override fun createCall() = webService.getLocations()

                override fun onFetchFailed() {
                    locationsListRateLimit.reset("")
                }
            }.asLiveData()
}
