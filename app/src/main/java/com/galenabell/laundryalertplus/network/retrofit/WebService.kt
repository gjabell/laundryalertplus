package com.galenabell.laundryalertplus.network.retrofit

import android.arch.lifecycle.LiveData
import com.galenabell.laundryalertplus.models.model.Location
import com.galenabell.laundryalertplus.models.model.Machine
import com.galenabell.laundryalertplus.models.model.Room
import com.galenabell.laundryalertplus.network.ApiResponse
import retrofit2.http.GET
import retrofit2.http.Path

interface WebService {
    @GET("/locations")
    fun getLocations(): LiveData<ApiResponse<List<Location>>>

    @GET("/locations/{code}/rooms")
    fun getRooms(@Path("code") code: String): LiveData<ApiResponse<List<Room>>>

    @GET("/rooms/{name}/machines")
    fun getMachines(@Path("name") name: String): LiveData<ApiResponse<List<Machine>>>
}
