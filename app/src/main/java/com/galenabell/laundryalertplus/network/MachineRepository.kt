package com.galenabell.laundryalertplus.network

import android.arch.lifecycle.LiveData
import com.galenabell.laundryalertplus.AppExecutors
import com.galenabell.laundryalertplus.database.dao.MachineDao
import com.galenabell.laundryalertplus.models.model.Machine
import com.galenabell.laundryalertplus.models.model.Resource
import com.galenabell.laundryalertplus.network.retrofit.WebService
import com.galenabell.laundryalertplus.util.RateLimiter
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class MachineRepository @Inject constructor(
        private val appExecutors: AppExecutors,
        private val machineDao: MachineDao,
        private val webService: WebService
) {
    private val machinesListRateLimit = RateLimiter<String>(1, TimeUnit.MINUTES)

    fun loadMachines(name: String): LiveData<Resource<List<Machine>>> =
            object : NetworkBoundResource<List<Machine>, List<Machine>>(appExecutors) {
                override fun saveCallResult(item: List<Machine>) {
                    machineDao.insertMachines(item)
                }

                override fun shouldFetch(data: List<Machine>?): Boolean {
                    return data == null || data.isEmpty() || machinesListRateLimit.shouldFetch(name)
                }

                override fun loadFromDb(): LiveData<List<Machine>> = machineDao.getMachines(name)

                override fun createCall(): LiveData<ApiResponse<List<Machine>>> = webService.getMachines(name)

                override fun onFetchFailed() {
                    machinesListRateLimit.reset(name)
                }
            }.asLiveData()
}
