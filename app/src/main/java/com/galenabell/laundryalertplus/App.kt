package com.galenabell.laundryalertplus

import android.app.Activity
import android.app.Application
import com.facebook.stetho.Stetho
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.galenabell.laundryalertplus.di.AppInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import okhttp3.OkHttpClient
import javax.inject.Inject

open class App : Application(), HasActivityInjector {
    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    override fun onCreate() {
        super.onCreate()
        AppInjector.init(this)
        initStetho()
    }

    private fun initStetho() {
        Stetho.initializeWithDefaults(this)
        OkHttpClient.Builder()
                .addNetworkInterceptor(StethoInterceptor())
                .build()
    }

    override fun activityInjector() = dispatchingAndroidInjector
}
