# LaundryAlertPlus

Proof-of-concept Android app for the LaundryAlert service (since the default
Android one is ugly and unstable). This app is incomplete and will not be completed
unless permission is given by Advanced Laundry Devices, Inc. It requires a [proxy
API](https://gitlab.com/gjabell/laundryalertcache) for turning the LaundryAlert
API into more manageable endpoints.

**Disclaimer: This is an unofficial, proof-of-concept app and is in no way affiliated
with Advanced Laundry Devices, Inc. The LaundryAlert terms of use forbid the use
of their API without permission. I am not responsible for breach of these terms
of use as a result of using this software.**